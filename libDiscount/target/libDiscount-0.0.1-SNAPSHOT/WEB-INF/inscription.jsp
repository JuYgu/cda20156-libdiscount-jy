<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false"%>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/connexion.css" rel="stylesheet">
    </head>
    <body>
  <div id="container">

            <!-- zone de connexion -->
            
            <form action="inscription" method="POST">
                <h1>Inscription</h1>
                
                <label><b>Login</b></label>
                <input type="text" placeholder="Entrer le nom d'utilisateur" name="login" value="${user.login }" >
                <span class="erreur">${inscrForm.erreurs['login']}</span>

                <label><b>Mot de passe</b></label>
                <input type="password" placeholder="Entrer le mot de passe" name="password" value="${user.password}" >
                <span class="erreur">${inscrForm.erreurs['password']}</span>
                
                <label><b>Librairie</b></label>
                <input type="text" placeholder="Entrer le nom de votre Librairie" name="librairie" value="${user.librairie}" >
                <span class="erreur">${inscrForm.erreurs['librairie']}</span>
                
                <label><b>Adresse</b></label>
                <input type="text" placeholder="Entrer votre adresse" name="adresse" value="${user.adresse}" >
                <span class="erreur">${inscrForm.erreurs['adresse']}</span>
                
                <label><b>Mail</b></label>
                <input type="text" placeholder="Entrer votre mail" name="mail" value="${user.mail}" >
                <span class="erreur">${inscrForm.erreurs['mail']}</span>
                
                <label><b>Nom </b></label>
                <input type="text" placeholder="Entrer votre nom" name="nom" value="${user.nom}" >
                <span class="erreur">${inscrForm.erreurs['nom']}</span>
                
                <label><b>Prenom</b></label>
                <input type="text" placeholder="Entrer votre prénom" name="prenom" value="${user.prenom}" >
                <span class="erreur">${inscrForm.erreurs['prenom']}</span>

                <input  class="button" type="submit" id='submit' value='LOGIN' >
                 <p class="${empty form.erreurs ? 'succes' : 'erreur'}" style="color : red">${inscrForm.resultat}</p> 
            </form>
        </div>
    </body>
</html>
