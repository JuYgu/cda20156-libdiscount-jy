<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
            <%@ taglib prefix="c" 
          uri="http://java.sun.com/jsp/jstl/core" %>
              <%@ include file="nav.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	
	<div class="recherche">
	<form action="accueil" method="GET">
			<label for="site-search">Rechercher une annonce :</label>
			<select name="option">
				<option  value="niveau"> Niveau Scolaire<option/>
				<option  value="ville"> Ville<option/>
				<option  value="nom"> Nom<option/>
				<option  value="all"> Toutes les annonces<option/>
			</select>
			<input type="search" id="site-search" name="critere" aria-label="Search through site content">
			<input  type="submit" value="Rechercher">
	</form>
	</div>

	<c:forEach var="annonce" items="${allAnnonces}">
		<div class="annonce">
			<div class="lab">
			heellloooo 
				<label><b>Titre</b></label>
				<p> ${annonce.nom}</p>
			</div>
			<div class="lab">
				<p> ${annonce.niveauScolaire.niveau}</p> 
			</div>
			<div class="lab">
				<label><b>Maison Edition</b></label>
				<p> ${annonce.maisonEdition}</p>
			</div>
			<div class="lab">
				<label><b>Prix</b></label>
				<p> ${annonce.prixUnitaire}</p>
			</div>
			<div class="lab">
				<label><b>Remise</b></label>
				<p> ${annonce.remise}</p>
			</div>
			<form action="accueil" method="GET">
				<input type='hidden' name='id_annonce'  value="${annonce.id_annonce}" />
				<input  type="submit" value="Voir">
			</form>
	</c:forEach>
</body>
</html>