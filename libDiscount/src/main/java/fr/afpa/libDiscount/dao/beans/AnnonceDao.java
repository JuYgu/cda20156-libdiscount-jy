package fr.afpa.libDiscount.dao.beans;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Formula;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor

@Entity
@NamedQueries({
	@NamedQuery(name="getUserAnnonces",query="select a from AnnonceDao a where a.user.id_user=:id_user"),
	@NamedQuery(name="getAnnoncesByNom",query="select a from AnnonceDao a where a.nom like concat( '%', :nom, '%')")

})

public class AnnonceDao {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="annonce_id_generator")
	@SequenceGenerator(name = "annonce_id_generator", sequenceName = "annonce_seq", allocationSize = 1, initialValue = 1)
	@Column(name="id_annonce")
	private long id_annonce;
	
	@Column(nullable = false, length = 13)
	private String isbn;
	
	@Column(nullable = false)
	private String nom;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fk_id_niveauScolaire", referencedColumnName = "id_niveauScolaire")
	private NiveauScolaireDao niveauScolaire;
	
	@Column(nullable = false)
	private String maisonEdition;
	
	@Column(nullable = false)
	private LocalDate dateEdition;
	
	@Column(nullable = false)
	private float prixUnitaire;
	
	@Column(nullable = false)
	private int quantite;
	
	@Column(nullable = false)
	private float remise;
	
	@Column(nullable = false)
	private float prixTotal;
	
	@OneToMany (mappedBy = "annonce", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
	private List<PhotoDao> photos;
	
	@Column(nullable = false)
	private boolean activation;
	
	@ManyToOne (fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST,
			CascadeType.DETACH,
			CascadeType.REFRESH,
			 } )
	@JoinColumn (name= "fk_id_user", referencedColumnName = "id_user")
	private UserDao user;

	
	
	
	
	public long getId_annonce() {
		return id_annonce;
	}

	public void setId_annonce(long id_annonce) {
		this.id_annonce = id_annonce;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public NiveauScolaireDao getNiveauScolaire() {
		return niveauScolaire;
	}

	public void setNiveauScolaire(NiveauScolaireDao niveauScolaire) {
		this.niveauScolaire = niveauScolaire;
	}

	public String getMaisonEdition() {
		return maisonEdition;
	}

	public void setMaisonEdition(String maisonEdition) {
		this.maisonEdition = maisonEdition;
	}

	public LocalDate getDateEdition() {
		return dateEdition;
	}

	public void setDateEdition(LocalDate dateEdition) {
		this.dateEdition = dateEdition;
	}

	public float getPrixUnitaire() {
		return prixUnitaire;
	}

	public void setPrixUnitaire(float prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public float getRemise() {
		return remise;
	}

	public void setRemise(float remise) {
		this.remise = remise;
	}

	@Formula("(( PrixUnitaire * Remise ) / 100 )* Quantite")
	public float getPrixTotal() {
		return prixTotal;
	}

	public void setPrixTotal(float prixTotal) {
		this.prixTotal = prixTotal;
	}

	public List<PhotoDao> getPhotos() {
		return photos;
	}

	public void setPhotos(List<PhotoDao> photos) {
		this.photos = photos;
	}

	public boolean isActivation() {
		return activation;
	}

	public void setActivation(boolean activation) {
		this.activation = activation;
	}

	public UserDao getUser() {
		return user;
	}

	public void setUser(UserDao user) {
		this.user = user;
	}
	
	
	

}
