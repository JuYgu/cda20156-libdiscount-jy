package fr.afpa.libDiscount.dao.beans;

import java.awt.Window.Type;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity
public class PhotoDao {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="photo_id_generator")
	@SequenceGenerator(name = "photo_id_generator", sequenceName = "photo_seq", allocationSize = 1, initialValue = 1)
	@Column(name="id_photo")
	private long id_photo;
	
	@Column(nullable = false)
	private String description;
	
	
	@Column(nullable = false)
	private String path;

	@Column(nullable = false)
	private String nom;
	
	@Column(nullable = false)
	private boolean activation;
	
	@ManyToOne (cascade = {CascadeType.ALL})
	@JoinColumn (name= "fk_id_annonce", referencedColumnName = "id_annonce")
	private AnnonceDao annonce;
}
