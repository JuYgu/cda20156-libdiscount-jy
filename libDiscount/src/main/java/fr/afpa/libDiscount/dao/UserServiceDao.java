package fr.afpa.libDiscount.dao;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.libDiscount.dao.beans.AnnonceDao;
import fr.afpa.libDiscount.dao.beans.UserDao;
import fr.afpa.libDiscount.dao.session.HibernateUtils;
import fr.afpa.libDiscount.dto.TransformToDto;
import fr.afpa.libDiscount.dto.beans.AnnonceDto;
import fr.afpa.libDiscount.dto.beans.UserDto;
import fr.afpa.libDiscount.model.beans.UserCriteria;

public class UserServiceDao {

	private Session s;
	private TransformToDto tDto;
	
	public UserServiceDao() {
		tDto = new TransformToDto();
	}
	
	
	/**
	 * Crée un User dans la base de donnée
	 * @param uDao
	 * @return
	 */
	public UserDto create(UserDao uDao) {
		
		s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		s.save(uDao);
		tx.commit();
		
		s.close();
		return tDto.transformUsertoDto(uDao);
		
	}
	
	public UserDto getUserByCriteria(UserCriteria criteria) {
		
		UserDto userDto = null;
		UserDao user = null;
		List<UserDao> userList = new ArrayList<UserDao>();
		
		s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		 
		if(criteria.getId() != 0) { 
			
			user = (UserDao) s.get(UserDao.class, criteria.getId());  
			tx.commit();
			
		 } else if (criteria.getLogin() != null) {
			 
			 Query q = s.getNamedQuery("getUserByLogin");
			 q.setParameter("login", criteria.getLogin());
			 try {
				 user = (UserDao) q.getSingleResult();
				 tx.commit();
			 }catch( NoResultException e) {

			 }
		 }
		
		 if(user != null) {
			userDto = tDto.transformUsertoDto(user);
		 }
		 
		 s.close();
		 
		 return userDto;
	}


	public List<AnnonceDto> getUserAnnonces(UserCriteria criteria) {
		
		 s = HibernateUtils.getSession();
		 Transaction tx = s.beginTransaction();
		 Query q = s.getNamedQuery("getUserAnnonces");
		 q.setParameter("id_user", criteria.getId());
		 List<AnnonceDao> annoncesDao = (List<AnnonceDao>)q.getResultList();
		 tx.commit();
		 s.close();
		 System.out.println(annoncesDao);

		 return tDto.listeAnnonceToDto(annoncesDao);
		 

	}


	public UserDto update(UserDao uDao) {
		
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		s.merge(uDao);
		tx.commit();
		s.close();
		
		return tDto.transformUsertoDto(uDao);
	}


	public List<UserDto> getAllUsers() {
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		
		List<UserDao> userList = s.createQuery("from UserDao", UserDao.class).getResultList();
		
		TransformToDto tDto = new TransformToDto();
		return tDto.transformUsertoDto(userList);
	}



	
	
	
	
	
}
