package fr.afpa.libDiscount.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.libDiscount.dao.beans.NiveauScolaireDao;
import fr.afpa.libDiscount.dao.session.HibernateUtils;
import fr.afpa.libDiscount.dto.TransformToDto;
import fr.afpa.libDiscount.dto.beans.NiveauScolaireDto;

public class NiveauScolaireServiceDao {
	

	public List<NiveauScolaireDto> getNiveaux() {
		
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		
		List<NiveauScolaireDao> nvDao = s.createQuery("from NiveauScolaireDao", NiveauScolaireDao.class).getResultList();
		
		TransformToDto tDto = new TransformToDto();
		return tDto.listeNiveauScolaireToDto(nvDao);
		

	}

}
