package fr.afpa.libDiscount.dao.beans;



import javax.persistence.Entity;
import javax.persistence.NamedQuery;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter


@Entity
@NamedQuery(name="getAdminByLogin",query="select u from AdminDao u where u.login=:login")
public class AdminDao extends UserDao {

}
