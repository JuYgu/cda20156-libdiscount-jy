package fr.afpa.libDiscount.dao.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity
@NamedQuery(name="getUserByLogin",query="select u from UserDao u where u.login=:login")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS )
public class UserDao {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="user_id_generator")
	@SequenceGenerator(name = "user_id_generator", sequenceName = "user_seq", allocationSize = 1, initialValue = 1)
	@Column(name="id_user", columnDefinition ="bigint")
	private long id_user;
	
	@Column(nullable = false)
	private String nom;
	
	@Column(nullable = false)
	private String prenom;
	
	@Column(nullable = false)
	private String librairie;
	
	@Column(nullable = false)
	private String adresse;
	
	@Column(nullable = false)
	private String mail;
	
	@Column(nullable = false)
	private String login;
	
	@Column(nullable = false)
	private String password;
	
	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = { CascadeType.ALL }  )
	private List<AnnonceDao> annonces;
	
	@Column(nullable = false)
	private boolean activation;
}
