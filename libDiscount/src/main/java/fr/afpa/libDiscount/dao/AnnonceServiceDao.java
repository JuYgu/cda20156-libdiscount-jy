package fr.afpa.libDiscount.dao;

import java.util.ArrayList;
import java.util.List;




import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import fr.afpa.libDiscount.dao.beans.AnnonceDao;
import fr.afpa.libDiscount.dao.beans.NiveauScolaireDao;
import fr.afpa.libDiscount.dao.session.HibernateUtils;
import fr.afpa.libDiscount.dto.TransformToDto;
import fr.afpa.libDiscount.dto.beans.AnnonceDto;
import fr.afpa.libDiscount.model.beans.AnnonceCriteria;

public class AnnonceServiceDao {

	private Session s;
	private TransformToDto tDto;
	
	public AnnonceServiceDao() {
		tDto = new TransformToDto();
	}

	public boolean create(AnnonceDao daoAnnonce) {
		
		s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		s.save(daoAnnonce);
		tx.commit();
		s.close();
		 
		return true;

	}

	public List<AnnonceDto> getAnnoncesByCriteria(AnnonceCriteria criteria) {
		
		List<AnnonceDto> annonceDto = null;
		List<AnnonceDao> annonceDao = new ArrayList<AnnonceDao>();
		Query q = null;
		s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		
		if(criteria.getVille() != null) {

			q = s.createQuery("FROM AnnonceDao a INNER JOIN UserDao u ON a.user.id_user = u.id_user WHERE u.adresse like concat('%',:ville,'%') ");
			q.setParameter("ville", criteria.getVille());
			System.err.println(criteria.getVille());
			List<Object[]> list = (List<Object[]>) q.list();
			for(Object[] o : list) {
				AnnonceDao ann = (AnnonceDao) o[0];
				
				annonceDao.add(ann);
			}

		}else if(criteria.getLibrairie() != null) {

			q = s.createQuery("SELECT a FROM AnnonceDao a INNER JOIN UserDao u ON a.user.id_user = u.id_user WHERE u.librairie like concat('%',:librairie,'%') ");
			q.setParameter("librairie", criteria.getLibrairie());
			
		}else if (criteria.getNiveau() != null) {
			q = s.createQuery("SELECT a FROM AnnonceDao a INNER JOIN NiveauScolaireDao nv ON a.niveauScolaire.id_niveauScolaire = nv.id_niveauScolaire WHERE nv.niveau like concat('%',:niveau,'%') ");
			q.setParameter("niveau", criteria.getNiveau());
			 annonceDao = (List<AnnonceDao>) q.getResultList();
			
		}else if (criteria.getNom() != null) {
			q = s.getNamedQuery("getAnnoncesByNom");
			q.setParameter("nom",  criteria.getNom());
			 annonceDao = (List<AnnonceDao>) q.getResultList();
		}
		

		tx.commit();
		s.close();
		System.err.println(annonceDao);
		if (!annonceDao.isEmpty()) {
			annonceDto = tDto.listeAnnonceToDto(annonceDao);
		}
		
		return annonceDto;
	}

	public boolean update(AnnonceDao aDao) {
		
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		s.merge(aDao);
		tx.commit();
		s.close();
		
		return true;
	}

	public List<AnnonceDto> getAllAnnonces() {
		
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		
		List<AnnonceDao> annonceDao = s.createQuery("from AnnonceDao", AnnonceDao.class).getResultList();
		
		TransformToDto tDto = new TransformToDto();
		return tDto.listeAnnonceToDto(annonceDao);
		
	}

	public void delete(AnnonceDao an) {
		
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		if(an.getPhotos() != null && !an.getPhotos().isEmpty()  ) {
			s.delete(an.getPhotos().get(0));
		}

		s.delete(an);
		tx.commit();
		s.close();
		
	}

	
	
}
