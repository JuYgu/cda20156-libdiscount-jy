package fr.afpa.libDiscount.dao.beans;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@ToString
@Entity
public class NiveauScolaireDao {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="nscolaire_id_generator")
	@SequenceGenerator(name = "nscolaire_id_generator", sequenceName = "nscolaire_seq", allocationSize = 1, initialValue = 1)
	@Column(name="id_niveauScolaire")
	private long id_niveauScolaire;
	
	@Column(nullable = false)
	private String niveau;
	

}
