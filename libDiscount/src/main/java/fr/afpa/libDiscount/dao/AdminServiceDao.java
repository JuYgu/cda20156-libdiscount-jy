package fr.afpa.libDiscount.dao;

import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.libDiscount.dao.beans.AdminDao;
import fr.afpa.libDiscount.dao.session.HibernateUtils;
import fr.afpa.libDiscount.dto.TransformToDto;
import fr.afpa.libDiscount.dto.beans.AdminDto;
import fr.afpa.libDiscount.model.beans.UserCriteria;

public class AdminServiceDao {

	public AdminDto getAdmin(UserCriteria criteria) {
		
		AdminDao admin = new AdminDao();
		AdminDto adminDto = null;
		
		Session s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
			 
		 Query q = s.getNamedQuery("getAdminByLogin");
		 q.setParameter("login", criteria.getLogin());
		 
		 try {
			 admin = (AdminDao) q.getSingleResult();
			 tx.commit();
		 }catch( NoResultException e) {
			 e.printStackTrace();
		 }
		 
		
		 if(admin != null) {
			TransformToDto tDto = new TransformToDto(); 
			adminDto = tDto.transformAdmintoDto(admin);
		 }
		 
		 s.close();
		 
		 return adminDto;
	}

}
