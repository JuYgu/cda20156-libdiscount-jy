package fr.afpa.libDiscount.control.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.libDiscount.control.form.InscriptionForm;
import fr.afpa.libDiscount.dto.beans.NiveauScolaireDto;
import fr.afpa.libDiscount.model.GestionAnnonce;
import fr.afpa.libDiscount.model.GestionUser;
import fr.afpa.libDiscount.model.beans.UserBean;

/**
 * Servlet implementation class ControlProfil
 */
public class ControlProfil extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public static final String ATT_USER = "user";
    public static final String ATT_FORM = "UserForm";
    public static final String VUE = "/WEB-INF/profil.jsp";
    public static final String ATT_SESSION_USER = "sessionUtilisateur";
    
    private GestionUser gUser;
    private GestionAnnonce gAnnonce;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControlProfil() {
        super();
        gAnnonce = new GestionAnnonce();

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		HttpSession session = request.getSession();
		gUser = (GestionUser) session.getAttribute("gestionUser");
		ArrayList<NiveauScolaireDto> nvS = gAnnonce.getNiveauxScolaire();
		session.setAttribute( ATT_SESSION_USER, gUser.currentUser);
		session.setAttribute("gestionAnnonce", gAnnonce);
		session.setAttribute("nvScolaire", nvS);

		if("Modifier".equals(request.getParameter("modifier"))) {
			request.setAttribute("modification", true);
		}else {
			request.setAttribute("modification", false);
		}

		if("Validation".equals(request.getParameter("modifier"))) {

			InscriptionForm inscrForm = new InscriptionForm();
			UserBean user = inscrForm.inscriptionUser(request);
	        request.setAttribute( ATT_FORM, inscrForm );

			 if (inscrForm.getErreurs().isEmpty()) {

				 if(gUser.modifierprofil(user)){
					 session.setAttribute( ATT_SESSION_USER, gUser.currentUser );
				}else {

			        request.setAttribute( ATT_FORM, inscrForm );
				}
			 }

		
		}

		
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);

	}

}
