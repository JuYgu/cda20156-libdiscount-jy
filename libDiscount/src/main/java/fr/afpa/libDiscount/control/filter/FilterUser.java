package fr.afpa.libDiscount.control.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;

/**
 * Servlet Filter implementation class FilterUser
 */
public class FilterUser implements Filter {

	 private  static Logger logger = Logger.getLogger(FilterUser.class) ;

	    // méthodes init() et destroy()   
	   
	    private  void beforeProcessing(ServletRequest request, ServletResponse response) 
	    throws IOException, ServletException {
	   
	       HttpServletRequest httpRequest = (HttpServletRequest)request ;
	       String host = httpRequest.getRemoteHost() ;
	       String url = httpRequest.getRequestURL().toString() ;
	       String client = request.getRemoteHost(); 
	       System.out.println("L'hôte [" + host +  "] fait une requête sur [" + url +  ""
	       		+ "hote : "+ client+" ] ") ;
	      
	   }
	   

	   

	    public  void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		     
	    	  beforeProcessing(request, response) ;
		    	HttpServletRequest req = (HttpServletRequest) request;
		    	HttpServletResponse res = (HttpServletResponse) response;
		    	HttpSession session = req.getSession();
		    	 String chemin = req.getRequestURI().substring( req.getContextPath().length() );
		    	
		    	String path = req.getServletPath();
		    	
		    	if (session.getAttribute("sessionUtilisateur") != null || path.equals("/inscription") || path.equals("/connexion") || chemin.startsWith("/css")) {
		    		filterChain.doFilter(request, response);
		    	} else {
		    		 request.getRequestDispatcher( "/connexion" ).forward( request, response );
		    	}
	   }

		@Override
		public void destroy() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void init(FilterConfig arg0) throws ServletException {
			// TODO Auto-generated method stub
			
		}
}
