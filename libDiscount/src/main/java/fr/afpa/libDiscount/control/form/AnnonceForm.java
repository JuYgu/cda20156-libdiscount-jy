package fr.afpa.libDiscount.control.form;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import fr.afpa.libDiscount.dto.beans.AnnonceDto;
import fr.afpa.libDiscount.dto.beans.NiveauScolaireDto;
import fr.afpa.libDiscount.dto.beans.PhotoDto;
import fr.afpa.libDiscount.model.GestionUser;
import lombok.Setter;

public class AnnonceForm {
	private static final String CHAMP_NOM = "nom";
	private static final String CHAMP_ISBN = "isnb";
	private static final String CHAMP_NIVEAU = "niveau";
	private static final String CHAMP_MEDITION   = "medition";
	private static final String CHAMP_DEDITION   = "dedition";
	private static final String CHAMP_PRIXU   = "prixu";
	private static final String CHAMP_QUANTITE   = "quantite";
	private static final String CHAMP_REMISE   = "remise";
	private static final String CHAMP_DESCRIPTION   = "description";
                      // 10 ko
	@Setter
	private String resultat;
	private Map<String, String> erreurs;


	
	public AnnonceForm() {
		erreurs = new HashMap<String, String>();
	}
	
	public String getResultat() {
	    return resultat;
	}

	public Map<String, String> getErreurs() {
	    return erreurs;
	}
	
	public AnnonceDto creationAnnonce(HttpServletRequest request, HttpSession session, String uploadPath) {
		
		String nom     = getValeurChamp(request, CHAMP_NOM);
		String isbn  = getValeurChamp(request, CHAMP_ISBN);
		String niveau = request.getParameter( CHAMP_NIVEAU );
		System.out.println("Mon niveau" + niveau);
		String mEdition   = getValeurChamp(request, CHAMP_MEDITION );
		String dateEdition      = getValeurChamp(request, CHAMP_DEDITION );
		String prixu       = getValeurChamp(request, CHAMP_PRIXU );
		String quantite    = getValeurChamp(request, CHAMP_QUANTITE );
		String remise    = getValeurChamp(request, CHAMP_REMISE );
		String description    = getValeurChamp(request, CHAMP_DESCRIPTION );
		
		
		AnnonceDto annonce = new AnnonceDto();
		
		annonce.setNom(nom);
		annonce.setIsbn(isbn);
		System.out.println("prix u"+ prixu);
		annonce.setMaisonEdition(mEdition);
		annonce.setPrixUnitaire(Float.parseFloat(prixu));
		annonce.setQuantite(Integer.parseInt(quantite));
		annonce.setRemise(Float.parseFloat(remise));
		annonce.setPrixTotal(((annonce.getPrixUnitaire()/annonce.getRemise())*100)*annonce.getQuantite());
		annonce.setUser(GestionUser.currentUser);
		
		DateTimeFormatter formatter = null;
		if(dateEdition.indexOf('-') != -1) {
			System.out.println(dateEdition);
			dateEdition = dateEdition.replace('-', '/');
			System.out.println(dateEdition);
			formatter = DateTimeFormatter.ofPattern("yyyy/MM/d");
		}else {
			formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
		}
		annonce.setDateEdition(LocalDate.parse(dateEdition, formatter));
		
		List<NiveauScolaireDto> lNv = (List<NiveauScolaireDto>) session.getAttribute("nvScolaires");
		System.out.println( "mes niveaux scolaire"+lNv);
		NiveauScolaireDto niveauS = new NiveauScolaireDto();
		for(NiveauScolaireDto niv : lNv ) {
			if(niveau.equals(niv.getNiveau())) {
				niveauS = niv;
			}
		}
		annonce.setNiveauScolaire(niveauS);
		System.out.println(annonce.getNom());
		
		
		
		String fileName = "";
		String fullPath = "";
		
	  try {
		for ( Part part : request.getParts() ) {
		       String nameTest  = getFileName( part );
		        System.out.println(fileName);
		        if(nameTest.indexOf("Default") == -1) {
		        	fileName = nameTest;
		        	fullPath = uploadPath + File.separator + fileName;
			        part.write( fullPath );
		        }

		    }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PhotoDto photo = new PhotoDto();
		photo.setDescription(description);
		photo.setPath(fullPath);
		photo.setNom(fileName);
		photo.setAnnonce(annonce);
		
		List<PhotoDto> photos = new ArrayList<PhotoDto>();
		photos.add(photo);
		annonce.setPhotos(photos);
		
		
		return annonce;
		
	}

	private void validationNomMEdition(String champs)  throws Exception {
	    if ( champs == null || champs.length() < 2 ) {

	        throw new Exception( "ce champ doit faire minimum 2 caractere" );
	    }
	}

	private void validationISBN(String champs)  throws Exception {
		if ( champs == null || champs.length() < 13 || champs.length() > 13 ) {
			
			throw new Exception( "ce champ doit faire minimum 13 caractere" );
		}
	}
	
	
	
	private void validationDate( String date ) throws Exception {
	    if ( date != null ) {
	        if ( !date.matches( "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(-)(?:0?[13-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(-)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(-)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$" ) ) {
	            throw new Exception( "Merci de saisir une date valide (dd-mm-yyyy)." );
	        }
	    } else {
	        throw new Exception( "Merci de saisir une adresse mail." );
	    }
	}
	
	/*
	 * Ajoute un message correspondant au champ spécifié à la map des erreurs.
	 */
	private void setErreur( String champ, String message ) {
	    erreurs.put( champ, message );
	}

	/*
	 * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
	 * sinon.
	 */
	private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
	    String valeur = request.getParameter( nomChamp );
	    System.out.println("les champs sont "+request.getParameter( nomChamp ));
	    if ( valeur == null || valeur.trim().length() == 0 ) {
	        return null;
	    } else {
	        return valeur.trim();
	    }
	}
	
	
    private String getFileName( Part part ) {
        for ( String content : part.getHeader( "content-disposition" ).split( ";" ) ) {
            if ( content.trim().startsWith( "filename" ) )
                return content.substring( content.indexOf( "=" ) + 2, content.length() - 1 );
        }
        return "Default.file";
    }
}
