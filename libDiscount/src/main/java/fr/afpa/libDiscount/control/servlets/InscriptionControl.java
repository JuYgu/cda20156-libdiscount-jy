package fr.afpa.libDiscount.control.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.libDiscount.control.form.InscriptionForm;
import fr.afpa.libDiscount.model.GestionUser;
import fr.afpa.libDiscount.model.beans.UserBean;
import fr.afpa.libDiscount.model.beans.UserConnBean;

/**
 * Servlet implementation class InscriptionControl
 */
public class InscriptionControl extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public static final String ATT_USER = "user";
    public static final String ATT_FORM = "inscrForm";
    public static final String VUE = "/WEB-INF/inscription.jsp";
    public static final String VUE_PROFIL = "/profil";
    public static final String ATT_SESSION_USER = "sessionUtilisateur";
    private GestionUser gUser;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InscriptionControl() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String vue = "";
		
		HttpSession session = request.getSession();
		gUser = (GestionUser) session.getAttribute("gestionUser");
		if(gUser == null) {
			gUser = new GestionUser();
		}
		InscriptionForm inscrForm = new InscriptionForm();
		UserBean user = inscrForm.inscriptionUser(request);
		

        request.setAttribute( ATT_FORM, inscrForm );
        request.setAttribute( ATT_USER, user );
        
        if (inscrForm.getErreurs().isEmpty()) {

			if(gUser.inscription(user)){
				 session.setAttribute( ATT_SESSION_USER, gUser.currentUser );
				 vue = VUE_PROFIL;

			}else {
				inscrForm.setResultat("Le login indiqué n'est pas disponible");
		        request.setAttribute( ATT_FORM, inscrForm );
		        vue = VUE;

			}
		}else {
			vue = VUE;

		}
        
		this.getServletContext().getRequestDispatcher(vue).forward(request, response);
	}

}
