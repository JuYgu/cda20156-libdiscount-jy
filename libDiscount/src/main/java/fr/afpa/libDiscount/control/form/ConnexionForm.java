package fr.afpa.libDiscount.control.form;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import fr.afpa.libDiscount.model.beans.UserConnBean;
import lombok.Setter;

public class ConnexionForm {

	private static final String CHAMP_LOGIN = "login";
	private static final String CHAMP_PASSWORD = "password";
	private static final String CHAMP_CONF   = "confirmation";
	@Setter
	private String resultat;
	private Map<String, String> erreurs;


	
	public ConnexionForm() {
		erreurs = new HashMap<String, String>();
	}
	
	public String getResultat() {
	    return resultat;
	}

	public Map<String, String> getErreurs() {
	    return erreurs;
	}
	
	public UserConnBean connexionUser(HttpServletRequest request) {
		
		String login = getValeurChamp(request, CHAMP_LOGIN);
		String password = getValeurChamp(request, CHAMP_PASSWORD);
		String confirmation = getValeurChamp( request, CHAMP_CONF );
		
		UserConnBean userConn = new UserConnBean();
		
		try {
			validationLoginPassword(login);
		} catch(Exception e) {
			setErreur(CHAMP_LOGIN, e.getMessage());
		}
		userConn.setLogin(login);
		
		try {
			validationLoginPassword(password);
		} catch(Exception e) {
			setErreur(CHAMP_PASSWORD, e.getMessage());
		}
		userConn.setPassword(password);
	
		return userConn;
		
	}

	private void validationLoginPassword(String champs)  throws Exception {
	    if ( champs == null || champs.length() < 1 ) {
	    	System.out.println("C'est vide");
	        throw new Exception( "ce champ est vide" );
	    }
	}
	
	/*
	 * Ajoute un message correspondant au champ spécifié à la map des erreurs.
	 */
	private void setErreur( String champ, String message ) {
	    erreurs.put( champ, message );
	}

	/*
	 * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
	 * sinon.
	 */
	private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
	    String valeur = request.getParameter( nomChamp );
	    System.out.println(request.getParameter( nomChamp ));
	    if ( valeur == null || valeur.trim().length() == 0 ) {
	        return null;
	    } else {
	        return valeur.trim();
	    }
	}
	
	

}
