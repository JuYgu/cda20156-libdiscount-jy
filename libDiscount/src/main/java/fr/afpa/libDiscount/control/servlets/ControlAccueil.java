package fr.afpa.libDiscount.control.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.libDiscount.dto.beans.AnnonceDto;
import fr.afpa.libDiscount.model.GestionAnnonce;
import fr.afpa.libDiscount.model.GestionUser;
import fr.afpa.libDiscount.model.beans.AnnonceCriteria;

/**
 * Servlet implementation class ControlAccueil
 */
public class ControlAccueil extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public static final String ATT_SESSION_USER = "sessionUtilisateur";
    public static final String VUE_ACCUEIL = "/WEB-INF/accueil.jsp";
    public static final String VUE_ANNONCE = "/WEB-INF/annonce.jsp";
    public static final String ATT_SESSION_ANNONCEUSER_LIST = "annoncesUser";
    public static final String ALL_ANNONCES = "allAnnonces";
    
    private GestionUser gUser;
    private GestionAnnonce gAnnonce;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControlAccueil() {
        super();
        gAnnonce = new GestionAnnonce();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String vue = "";
		
		HttpSession session = request.getSession();
		List<AnnonceDto> allAnnonces = new ArrayList<AnnonceDto>();
		
		if ( gUser == null) {
			gUser = (GestionUser) session.getAttribute("gestionUser");
		}
		
		if( gAnnonce.getAllAnnonces()) {
			allAnnonces = gAnnonce.getAllAnnoncesUsers();
		}
		
		request.setAttribute(ALL_ANNONCES, allAnnonces);
		
		
		if(request.getParameter("id_annonce") != null && allAnnonces != null) {
			AnnonceDto annonce = new AnnonceDto();
			for ( AnnonceDto an : allAnnonces) {
				System.out.println(an);
				String an_id = new Long(an.getId_annonce()).toString();
				if(an_id.equals(request.getParameter("id_annonce")) ) {
					annonce = an;
					System.out.println("la current annon"+annonce);
					request.setAttribute("currentAnnonce", annonce);
					System.out.println("ok !!!!!!!");
					vue = VUE_ANNONCE;
				}
			}
			
		}else if ( request.getParameter("critere") != null && request.getParameter("option") != null){
			AnnonceCriteria criteria = new AnnonceCriteria();
			if ("ville".equals(request.getParameter("option"))){
				criteria.setVille( request.getParameter("critere"));
			}else if ("niveau".equals(request.getParameter("option"))) {
				criteria.setNiveau( request.getParameter("critere"));
			}else if ("nom".equals(request.getParameter("option"))) {
				criteria.setNom(request.getParameter("critere"));
			}
			if(gAnnonce.getAnnonces(criteria)) {
				request.setAttribute(ALL_ANNONCES, gAnnonce.getSearchAnnonces());
			}
			this.getServletContext().getRequestDispatcher(VUE_ACCUEIL).forward(request, response);
		}else if("all".equals(request.getParameter("option"))){
			request.setAttribute(ALL_ANNONCES,gAnnonce.getAllAnnoncesUsers());
			
		}else {
			vue = VUE_ACCUEIL;

		}
		
		
		this.getServletContext().getRequestDispatcher(vue).forward(request, response);
		
	}
	


}
