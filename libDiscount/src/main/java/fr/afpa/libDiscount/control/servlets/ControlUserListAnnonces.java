package fr.afpa.libDiscount.control.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.libDiscount.dto.beans.AnnonceDto;
import fr.afpa.libDiscount.model.GestionAnnonce;
import fr.afpa.libDiscount.model.GestionUser;

/**
 * Servlet implementation class ControlUserListAnnonces
 */
public class ControlUserListAnnonces extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public static final String VUE = "/WEB-INF/listAnnonces.jsp";
    public static final String VUE_ANNONCE = "/WEB-INF/monAnnonce.jsp";
    public static final String ATT_SESSION_USER = "sessionUtilisateur";
    public static final String ATT_SESSION_ANNONCEUSER_LIST = "annoncesUser";
    
    private GestionUser gUser;
    private GestionAnnonce gAnnonce;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControlUserListAnnonces() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String vue = "";
		
		HttpSession session = request.getSession();
		if ( gUser == null) {
			gUser = (GestionUser) session.getAttribute("gestionUser");
		}
		gUser.getUserAnnonces();
		session.setAttribute(ATT_SESSION_USER, GestionUser.currentUser);
		
		session.setAttribute(ATT_SESSION_ANNONCEUSER_LIST, GestionUser.currentUser.getAnnonces());
		System.out.println( GestionUser.currentUser.getAnnonces());
		System.out.println( GestionUser.currentUser);
		if(request.getParameter("id_annonce" ) != null) {
			AnnonceDto annonce = new AnnonceDto();
			for ( AnnonceDto an : gUser.currentUser.getAnnonces()) {
				System.out.println("l'user "+an);
				String an_id = new Long(an.getId_annonce()).toString();
				if(an_id.equals(request.getParameter("id_annonce")) ) {
					annonce = an;
					if(request.getParameter("supprimer") == null) {
						request.setAttribute("currentAnnonce", annonce);
						session.setAttribute("currentAnnonce", annonce);
						vue = VUE_ANNONCE;
					}else {
						GestionAnnonce gAnn = new GestionAnnonce();
						gAnn.delete(an);
						gUser.getUserAnnonces();
						session.setAttribute(ATT_SESSION_USER, gUser.currentUser);
						session.setAttribute(ATT_SESSION_ANNONCEUSER_LIST, GestionUser.currentUser.getAnnonces());
						vue = VUE;
					}
				}
			}
		}else {
			vue = VUE;

		}
		this.getServletContext().getRequestDispatcher(vue).forward(request, response);
		
	}
	

}
