package fr.afpa.libDiscount.control.servlets;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import fr.afpa.libDiscount.control.form.AnnonceForm;
import fr.afpa.libDiscount.dto.beans.AnnonceDto;
import fr.afpa.libDiscount.dto.beans.NiveauScolaireDto;
import fr.afpa.libDiscount.model.GestionAnnonce;
import fr.afpa.libDiscount.model.GestionUser;

/**
 * Servlet implementation class CreationAnnonceControl
 */

@MultipartConfig( fileSizeThreshold = 1024 * 1024, 
maxFileSize = 1024 * 1024 * 5,
maxRequestSize = 1024 * 1024 * 5 * 5 )
public class CreationAnnonceControl extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public static final String ATT_ANNONCE = "annonce";
    public static final String ATT_FORM = "AnnForm";
    public static final String VUE = "/WEB-INF/creationAnnonce.jsp";
    public static final String VUE_PROFIL = "/WEB-INF/profil.jsp";
    public static final String ATT_SESSION_USER = "sessionUtilisateur";

    public static String IMAGES_FOLDER;
    
    public String uploadPath;
    private GestionUser gUser;
    private GestionAnnonce gAnnonce;
       
    /**
     * @see HttpServlet#HttpServlet()
     */

    public CreationAnnonceControl() {
        super();
		gAnnonce = new GestionAnnonce();
    }
    
    public void init() throws ServletException {
    	IMAGES_FOLDER =  getServletContext().getRealPath("")+"\\images";
    	System.out.println(IMAGES_FOLDER);
        uploadPath = IMAGES_FOLDER ;
        System.out.println(uploadPath);
        File uploadDir = new File( uploadPath );
        if ( ! uploadDir.exists() ) uploadDir.mkdir();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

			ArrayList<NiveauScolaireDto> nvS = gAnnonce.getNiveauxScolaire();
			System.out.println("niveau" + nvS);
			HttpSession session = request.getSession();
			session.setAttribute("nvScolaires", nvS);
		
			this.getServletContext().getRequestDispatcher(VUE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String vue = "";
		
		HttpSession session = request.getSession();
		gUser = (GestionUser) session.getAttribute("gestionUser");

		AnnonceForm form = new AnnonceForm();
		AnnonceDto annonce = form.creationAnnonce(request, session, uploadPath); 
		
		if (gAnnonce.createAnnonce(annonce)) {
			vue = VUE_PROFIL;
	        request.setAttribute( ATT_FORM, form );
		}else {
	        vue = VUE;
		}
		
		this.getServletContext().getRequestDispatcher(vue).forward(request, response);
	}
	


}
