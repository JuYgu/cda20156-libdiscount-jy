package fr.afpa.libDiscount.control.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.libDiscount.control.form.AnnonceForm;
import fr.afpa.libDiscount.dto.beans.AnnonceDto;
import fr.afpa.libDiscount.model.GestionAnnonce;
import fr.afpa.libDiscount.model.GestionUser;

/**
 * Servlet implementation class ControlModificationAnnonce
 */
public class ControlModificationAnnonce extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public static final String ATT_ANNONCE = "annonce";
    public static final String ATT_FORM = "AnnForm";
    public static final String VUE = "/WEB-INF/creationAnnonce.jsp";
    public static final String ATT_SESSION_USER = "sessionUtilisateur";
    public static final String CHEMIN      = "chemin";
    private GestionUser gUser;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControlModificationAnnonce() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		
		String chemin = this.getServletConfig().getInitParameter( CHEMIN );
		HttpSession session = request.getSession();
		gUser = (GestionUser) session.getAttribute("gestionUser");

		AnnonceForm form = new AnnonceForm();
		AnnonceDto annonce = form.creationAnnonce(request, session, chemin); 
		annonce.setId_annonce(Long.parseLong(request.getParameter("id_annonce")));
		GestionAnnonce gAnnonce = new GestionAnnonce();
		if (gAnnonce.modifierAnnonce(annonce)) {

			if(gUser.getUserAnnonces()) {

				session.setAttribute(ATT_SESSION_USER, gUser.currentUser);
			}

		}
		response.sendRedirect(request.getContextPath() + "/mes-annonces");
		
	}

}
