package fr.afpa.libDiscount.control.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.afpa.libDiscount.control.form.ConnexionForm;
import fr.afpa.libDiscount.dto.beans.AdminDto;
import fr.afpa.libDiscount.model.GestionUser;
import fr.afpa.libDiscount.model.beans.UserConnBean;

/**
 * Servlet implementation class ConnexionControl
 */
public class ConnexionControl extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public static final String ATT_USER = "user";
    public static final String ATT_FORM = "connForm";
    public static final String VUE = "/WEB-INF/connexion.jsp";
    public static final String VUE_ADMIN = "/WEB-INF/admin/accueilAdmin.jsp";
    public static final String VUE_USER = "/WEB-INF/profil.jsp";
    public static final String ATT_SESSION_USER = "sessionUtilisateur";
    
    private GestionUser gUser;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConnexionControl() {
        super();
        gUser = new GestionUser();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher(VUE).forward( request, response );
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String vue = "";
		boolean admin = false;
		
		ConnexionForm connForm = new ConnexionForm();
		UserConnBean user = connForm.connexionUser(request);

		HttpSession session = request.getSession();
		session.setAttribute("gestionUser", gUser);
        request.setAttribute( ATT_FORM, connForm );
        request.setAttribute( ATT_USER, user );
        
		if (connForm.getErreurs().isEmpty()) {
			 if ("admin".equals(request.getParameter("login"))){
					if(gUser.adminConnexion(user)) {
						session.setAttribute(ATT_SESSION_USER, GestionUser.currentUser);
						session.setAttribute("admin", true);
						admin = true;

					}
			
			}else if(gUser.connexion(user)){
				 session.setAttribute( ATT_SESSION_USER, GestionUser.currentUser );
			     session.setAttribute("admin", false);
			     vue = VUE_USER;
			}else  {
			
				connForm.setResultat("Vos identifiants ne sont pas reconnus");
		        request.setAttribute( ATT_FORM, connForm );
		        vue = VUE;
			}
		}else {
            session.setAttribute( ATT_SESSION_USER, null );
            vue = VUE;
		}

		if(!admin) {
			this.getServletContext().getRequestDispatcher(vue).forward(request, response);

		}else {
			this.getServletContext().getRequestDispatcher("/admin").forward(request, response);

		}

		
	}

}
