package fr.afpa.libDiscount.control.form;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import fr.afpa.libDiscount.model.beans.UserBean;
import fr.afpa.libDiscount.model.beans.UserConnBean;
import lombok.Setter;

public class InscriptionForm {

	private static final String CHAMP_LOGIN = "login";
	private static final String CHAMP_PASSWORD = "password";
	private static final String CHAMP_LIBRAIRIE = "librairie";
	private static final String CHAMP_ADRESSE   = "adresse";
	private static final String CHAMP_MAIL   = "mail";
	private static final String CHAMP_NOM   = "nom";
	private static final String CHAMP_PRENOM   = "prenom";
	@Setter
	private String resultat;
	private Map<String, String> erreurs;


	
	public InscriptionForm() {
		erreurs = new HashMap<String, String>();
	}
	
	public String getResultat() {
	    return resultat;
	}

	public Map<String, String> getErreurs() {
	    return erreurs;
	}
	
	public UserBean inscriptionUser(HttpServletRequest request) {
		
		String login     = getValeurChamp(request, CHAMP_LOGIN);
		String password  = getValeurChamp(request, CHAMP_PASSWORD);
		String librairie = getValeurChamp(request, CHAMP_LIBRAIRIE );
		String adresse   = getValeurChamp(request, CHAMP_ADRESSE );
		String mail      = getValeurChamp(request, CHAMP_MAIL );
		String nom       = getValeurChamp(request, CHAMP_NOM );
		String prenom    = getValeurChamp(request, CHAMP_PRENOM );

		
		UserBean userBean = new UserBean();
		
		try {
			validationLoginPassword(login);
		} catch(Exception e) {
			setErreur(CHAMP_LOGIN, e.getMessage());
		}
		userBean.setLogin(login);
		
		try {
			validationLoginPassword(password);
		} catch(Exception e) {
			setErreur(CHAMP_PASSWORD, e.getMessage());
		}
		userBean.setPassword(password);
		
		try {
			validationLoginPassword(nom);
		} catch(Exception e) {
			setErreur(CHAMP_NOM, e.getMessage());
		}
		userBean.setNom(nom);
		
		try {
			validationLoginPassword(prenom);
		} catch(Exception e) {
			setErreur(CHAMP_PRENOM, e.getMessage());
		}
		userBean.setPrenom(prenom);
		
		try {
			validationLibrairie(librairie);
		} catch(Exception e) {
			setErreur(CHAMP_LIBRAIRIE, e.getMessage());
		}
		userBean.setLibrairie(librairie);

		try {
			validationAdresse(adresse);
		} catch(Exception e) {
			setErreur(CHAMP_ADRESSE, e.getMessage());
		}
		userBean.setAdresse(adresse);

		try {
			validationEmail(mail);
		} catch(Exception e) {
			setErreur(CHAMP_MAIL, e.getMessage());
		}
		userBean.setMail(mail);
		
		
		
		return userBean;
		
	}

	private void validationLoginPassword(String champs)  throws Exception {
	    if ( champs == null || champs.length() < 4 ) {

	        throw new Exception( "ce champ doit faire minimum 4 caractere" );
	    }
	}
	
	
	private void validationLibrairie(String champs)  throws Exception {
		if ( champs == null || champs.length() < 1) {
			
			throw new Exception( "ce champ ne doit pas être vide" );
		}
	}
	
	private void validationAdresse(String champs)  throws Exception {
		if ( champs == null || champs.length() < 10) {
			
			throw new Exception( "ce champ ne doit faire minimum 10 caracteres" );
		}
	}
	
	private void validationEmail( String email ) throws Exception {
	    if ( email != null ) {
	        if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
	            throw new Exception( "Merci de saisir une adresse mail valide." );
	        }
	    } else {
	        throw new Exception( "Merci de saisir une adresse mail." );
	    }
	}
	
	/*
	 * Ajoute un message correspondant au champ spécifié à la map des erreurs.
	 */
	private void setErreur( String champ, String message ) {
	    erreurs.put( champ, message );
	}

	/*
	 * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
	 * sinon.
	 */
	private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
	    String valeur = request.getParameter( nomChamp );
	    System.out.println("les champs sont "+request.getParameter( nomChamp ));
	    if ( valeur == null || valeur.trim().length() == 0 ) {
	        return null;
	    } else {
	        return valeur.trim();
	    }
	}
	
	

}
