package fr.afpa.libDiscount.control.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.libDiscount.dto.beans.AnnonceDto;
import fr.afpa.libDiscount.dto.beans.UserDto;
import fr.afpa.libDiscount.model.GestionAnnonce;
import fr.afpa.libDiscount.model.GestionUser;

/**
 * Servlet implementation class ControlAdmin
 */
public class ControlAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public static final String ATT_USERS_LIST = "userList";
    public static final String VUE_ADMIN = "/WEB-INF/admin/accueilAdmin.jsp"; 
    public static final String VUE_ADMIN_USER = "/WEB-INF/admin/adminUser.jsp"; 
    private GestionUser gUser;
    private String vue;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControlAdmin() {
        super();
        gUser = new GestionUser();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		vue = "";
		if(gUser.getAllUsers()) {
			request.setAttribute(ATT_USERS_LIST, gUser.getUserList());
		}
		
		
		if(request.getParameter("id_user") != null || request.getParameter("id_annonce") != null) {
			UserDto currentUser = new UserDto();
			for (UserDto user : gUser.getUserList()) {
				if(request.getParameter("id_user").equals(((Long)user.getId_user()).toString())) {
					currentUser = user;
					
					if(request.getParameter("activation") != null &&  request.getParameter("id_annonce") == null) {
						user.setActivation(Boolean.parseBoolean(request.getParameter("activation")));
						if(gUser.updateUserActivation(user)) {
							request.setAttribute(ATT_USERS_LIST, gUser.getUserList());
							
							vue = VUE_ADMIN;
						}
						
					} else if (request.getParameter("id_annonce") != null) {
						
						AnnonceDto annonce = new AnnonceDto();
						for (AnnonceDto ann : user.getAnnonces()) {
							if((((Long)ann.getId_annonce()).toString()).equals(request.getParameter("id_annonce"))){
								annonce = ann;
								annonce.setActivation(Boolean.parseBoolean(request.getParameter("activation")));
								GestionAnnonce gAnn = new GestionAnnonce();
								if(gAnn.modifierAnnonce(annonce)) {
									gUser.getAllUsers();
									request.setAttribute(ATT_USERS_LIST, gUser.getUserList());
									
									request.setAttribute("currentUser", currentUser);
									vue = VUE_ADMIN_USER;
								}
							}
						}
						
					}else {
						request.setAttribute("currentUser", currentUser);
						vue = VUE_ADMIN_USER;
					}
				}
				
			
			}
			
		}else {
			vue = VUE_ADMIN;
			
		}
		this.getServletContext().getRequestDispatcher(vue).forward(request, response);

		
	}

}
