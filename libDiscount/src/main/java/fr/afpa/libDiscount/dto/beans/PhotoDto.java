package fr.afpa.libDiscount.dto.beans;

import java.awt.Image;

import javax.imageio.ImageIO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PhotoDto {

	private long id_photo;
	private String description;
	private String path;
	private String nom;

	private boolean activation;
	@ToString.Exclude
	private AnnonceDto annonce;
}
