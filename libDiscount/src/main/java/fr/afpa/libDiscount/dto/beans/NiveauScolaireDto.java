package fr.afpa.libDiscount.dto.beans;



import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class NiveauScolaireDto {
	private long id_niveauScolaire;
	private String niveau;
}
