package fr.afpa.libDiscount.dto.beans;


import java.util.ArrayList;
import java.util.List;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@AllArgsConstructor
@ToString
public class UserDto {

	private long id_user;
	private String nom;
	private String prenom;
	private String librairie;
	private String adresse;
	private String mail;
	private String login;
	private String password;
	private List<AnnonceDto> annonces;
	private boolean activation;
	
	public UserDto() {
		annonces = new ArrayList<AnnonceDto>();
		activation = true;
	}
}
