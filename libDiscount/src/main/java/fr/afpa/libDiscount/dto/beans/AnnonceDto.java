package fr.afpa.libDiscount.dto.beans;

import java.time.LocalDate;
import java.util.List;



import fr.afpa.libDiscount.dao.beans.NiveauScolaireDao;
import fr.afpa.libDiscount.dao.beans.PhotoDao;
import fr.afpa.libDiscount.dao.beans.UserDao;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter

@AllArgsConstructor
@ToString
public class AnnonceDto {

	private long id_annonce;
	private String isbn;
	private String nom;
	private NiveauScolaireDto niveauScolaire;
	private String maisonEdition;
	private LocalDate dateEdition;
	private float prixUnitaire;
	private int quantite;
	private float remise;
	private float prixTotal;
	private List<PhotoDto> photos;
	private boolean activation;
	@ToString.Exclude
	private UserDto user;
	
	
	public AnnonceDto() {
		this.activation = true;
	}
}
