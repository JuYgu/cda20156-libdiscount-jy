package fr.afpa.libDiscount.dto;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

import fr.afpa.libDiscount.dao.beans.AdminDao;
import fr.afpa.libDiscount.dao.beans.AnnonceDao;
import fr.afpa.libDiscount.dao.beans.NiveauScolaireDao;
import fr.afpa.libDiscount.dao.beans.PhotoDao;
import fr.afpa.libDiscount.dao.beans.UserDao;
import fr.afpa.libDiscount.dto.beans.AdminDto;
import fr.afpa.libDiscount.dto.beans.AnnonceDto;
import fr.afpa.libDiscount.dto.beans.NiveauScolaireDto;
import fr.afpa.libDiscount.dto.beans.PhotoDto;
import fr.afpa.libDiscount.dto.beans.UserDto;

public class TransformToDto {

	
	
	/**
	 * Tranforme une liste users en DTO
	 * @param daoUsers
	 * @return
	 */
	public List<UserDto> transformUsertoDto( List<UserDao> daoUsers){
		ModelMapper mp = new ModelMapper();
		List<UserDto> dtoUsers = new ArrayList<>();
		for( UserDao daoUser : daoUsers) {
			UserDto user = mp.map(daoUser, UserDto.class);
			dtoUsers.add(user);
		}
		return dtoUsers;
	}
	
	
	/**
	 * Transforme un utilisateur en DTO
	 * @param daoUser
	 * @return
	 */
	public UserDto transformUsertoDto( UserDao daoUser){
		ModelMapper mp = new ModelMapper();
		UserDto user = mp.map(daoUser, UserDto.class);
		return user;	
	}
	
	/**
	 * Tranforme une annonce en DTO
	 * @param Dao
	 * @return
	 */
	public AnnonceDto annonceToDto(AnnonceDao daoAnnonce) {
		ModelMapper mp = new ModelMapper();
		AnnonceDto user = mp.map(daoAnnonce, AnnonceDto.class);
		return user;	
	}
	
	/**
	 * Transforme une liste d'annonceDao en DTO
	 * @param daoAnnonces
	 * @return
	 */
	public  List<AnnonceDto> listeAnnonceToDto(List<AnnonceDao> daoAnnonces){
		ModelMapper mp = new ModelMapper();
		List<AnnonceDto> dtoAnnonces = new ArrayList<>();
		for( AnnonceDao daoAnnonce : daoAnnonces) {

			AnnonceDto annonceDto = new AnnonceDto();
			annonceDto = mp.map(daoAnnonce, AnnonceDto.class);
			dtoAnnonces.add(annonceDto);
		}
		return dtoAnnonces;
	}


	public List<NiveauScolaireDto> listeNiveauScolaireToDto(List<NiveauScolaireDao> nvDao) {
		List<NiveauScolaireDto> dtoNv = new ArrayList<>();
		for( NiveauScolaireDao daoNv : nvDao) {
			NiveauScolaireDto nvDto = new NiveauScolaireDto();
			nvDto.setId_niveauScolaire(daoNv.getId_niveauScolaire());
			nvDto.setNiveau(daoNv.getNiveau());
			dtoNv.add(nvDto);
		}
		return dtoNv;		
	}


	public AdminDto transformAdmintoDto(AdminDao admin) {
		ModelMapper mp = new ModelMapper();
		AdminDto adminDto = mp.map(admin, AdminDto.class);
		return adminDto;
	}
	
	
}
