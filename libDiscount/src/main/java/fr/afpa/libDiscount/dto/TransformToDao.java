package fr.afpa.libDiscount.dto;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;

import fr.afpa.libDiscount.dao.beans.AdminDao;
import fr.afpa.libDiscount.dao.beans.AnnonceDao;
import fr.afpa.libDiscount.dao.beans.PhotoDao;
import fr.afpa.libDiscount.dao.beans.UserDao;
import fr.afpa.libDiscount.dto.beans.AnnonceDto;
import fr.afpa.libDiscount.dto.beans.PhotoDto;
import fr.afpa.libDiscount.dto.beans.UserDto;
import fr.afpa.libDiscount.model.beans.UserBean;

public class TransformToDao {

	
	public List<UserDao> transformUserstoDao( List<UserDto> dtoUsers){
		
		ModelMapper mp = new ModelMapper();
		List<UserDao> daoUsers = new ArrayList<>();
		for( UserDao daoUser : daoUsers) {
			UserDao user = mp.map(daoUser, UserDao.class);
			daoUsers.add(user);
		}
		
		return daoUsers;
		
	}
	
	public UserDao transformUsertoDao( UserDto dtoUser){
		ModelMapper mp = new ModelMapper();
		UserDao user = mp.map(dtoUser, UserDao.class);
		return user;	
	}
	
	public UserDao transformUserBeantoDao( UserBean userBean){
		ModelMapper mp = new ModelMapper();
		UserDao user = mp.map(userBean, UserDao.class);
		return user;	
	}

	public AdminDao transformUserBeantoAdminDao( UserBean userBean){
		ModelMapper mp = new ModelMapper();
		AdminDao admin = mp.map(userBean, AdminDao.class);
		return admin;	
	}
	
	
	/**
	 * Tranforme une annonce en DAO
	 * @param Dao
	 * @return
	 */
	public AnnonceDao transformAnnonceToDao(AnnonceDto dtoAnnonce) {
		ModelMapper mp = new ModelMapper();
		
		AnnonceDao annonce = mp.map(dtoAnnonce, AnnonceDao.class);
		PhotoDao pDao = null;
		if(dtoAnnonce.getPhotos() != null && !dtoAnnonce.getPhotos().isEmpty()  ) {
			PhotoDto pDto = dtoAnnonce.getPhotos().get(0);
			pDao = mp.map(pDto, PhotoDao.class);
			List<PhotoDao> photos = new ArrayList<PhotoDao>();
			photos.add(pDao);
			annonce.setPhotos(photos);
		}

		


		return annonce;	
	}
}
