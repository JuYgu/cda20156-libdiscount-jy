package fr.afpa.libDiscount.dto.beans;

import java.time.LocalDate;
import java.util.List;

import fr.afpa.libDiscount.dao.beans.UserDao;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public class AdminDto extends UserDto {

}
