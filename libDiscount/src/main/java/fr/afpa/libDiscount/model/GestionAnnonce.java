package fr.afpa.libDiscount.model;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.libDiscount.dao.AnnonceServiceDao;
import fr.afpa.libDiscount.dao.NiveauScolaireServiceDao;
import fr.afpa.libDiscount.dao.beans.AnnonceDao;
import fr.afpa.libDiscount.dao.beans.NiveauScolaireDao;
import fr.afpa.libDiscount.dto.TransformToDao;
import fr.afpa.libDiscount.dto.beans.AnnonceDto;
import fr.afpa.libDiscount.dto.beans.NiveauScolaireDto;
import fr.afpa.libDiscount.model.beans.AnnonceCriteria;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GestionAnnonce {

	private List<AnnonceDto> currentAnnonces;
	private List<AnnonceDto> searchAnnonces;
	private List<AnnonceDto> allAnnoncesUsers;
	private AnnonceServiceDao aServ;
	private NiveauScolaireServiceDao nvServ;
	private TransformToDao tDao;
	
	public GestionAnnonce() {
		aServ = new AnnonceServiceDao();
		tDao = new TransformToDao();
		//currentAnnonces = new ArrayList<AnnonceDto>();
	}
	
	
	public boolean createAnnonce(AnnonceDto annonce) {
		
		AnnonceDao daoAnnonce = tDao.transformAnnonceToDao(annonce);
		
		return aServ.create(daoAnnonce);
		
	}
	
	public boolean getAnnonces(AnnonceCriteria criteria) {
		
		searchAnnonces = aServ.getAnnoncesByCriteria(criteria);
		if(searchAnnonces != null) {
			return true;
		}else {
			return false;
		}
	}
	
	public void deconnexion() {
		
		searchAnnonces = null;
	}
	
	public boolean modifierAnnonce(AnnonceDto aDto) {
		
		AnnonceDao aDao = tDao.transformAnnonceToDao(aDto);
		return aServ.update(aDao);
		
	}


	public ArrayList<NiveauScolaireDto> getNiveauxScolaire() {
		nvServ = new NiveauScolaireServiceDao();
		ArrayList<NiveauScolaireDto> nvL = (ArrayList<NiveauScolaireDto>) nvServ.getNiveaux();
		return  nvL;
	}
	
	public boolean getAllAnnonces() {
		
		allAnnoncesUsers = aServ.getAllAnnonces();
		if(allAnnoncesUsers != null) {
			return true;
		}else {
			return false;
		}
	}


	public void delete(AnnonceDto aDto) {
		AnnonceDao aDao = tDao.transformAnnonceToDao(aDto);
		
		aServ.delete(aDao);
	}



	
}
