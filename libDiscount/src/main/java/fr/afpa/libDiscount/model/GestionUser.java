package fr.afpa.libDiscount.model;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.libDiscount.dao.AdminServiceDao;
import fr.afpa.libDiscount.dao.UserServiceDao;
import fr.afpa.libDiscount.dao.beans.UserDao;
import fr.afpa.libDiscount.dto.TransformToDao;
import fr.afpa.libDiscount.dto.beans.AdminDto;
import fr.afpa.libDiscount.dto.beans.UserDto;
import fr.afpa.libDiscount.model.beans.UserBean;
import fr.afpa.libDiscount.model.beans.UserConnBean;
import fr.afpa.libDiscount.model.beans.UserCriteria;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GestionUser {

	public static UserDto currentUser;
	private AdminDto currentAdmin;
	private UserServiceDao uServ;
	private TransformToDao tDao;
	private List<UserDto> userList;
	

	
	public GestionUser() {
		uServ = new UserServiceDao();
		tDao = new TransformToDao();
		currentUser = new UserDto();
		userList = new ArrayList<UserDto>();
	}
	
	public boolean connexion(UserConnBean uConn) {
		
		UserCriteria criteria = new UserCriteria();
		criteria.setLogin(uConn.getLogin());
		UserDto uToCompare = uServ.getUserByCriteria(criteria);
		if(uToCompare != null && uToCompare.getPassword().equals(uConn.getPassword())) {
			currentUser = uToCompare;
			return true;
			
		} else {
			return false;
		}
	}
	
	public void deconnexion() {
		
		currentUser = null;
	}
	
	
	public boolean inscription(UserBean uBean) {
		
		UserConnBean uCheck = new UserConnBean();
		UserCriteria criteria = new UserCriteria();
		criteria.setLogin(uCheck.getLogin());
		
		UserDto uToCompare = uServ.getUserByCriteria(criteria);
		System.out.println("avant boucle");
		if (uToCompare == null) {
			
			UserDao uDao = tDao.transformUserBeantoDao(uBean);
			currentUser = uServ.create(uDao);
			return true;
		}else {
			return false;
		}
	}
	
	public boolean getUserAnnonces() {
		UserCriteria criteria = new UserCriteria();
		criteria.setId(currentUser.getId_user());
		
		currentUser.setAnnonces(uServ.getUserAnnonces(criteria));
		System.out.println("dans le service User"+currentUser);
		
		if(currentUser.getAnnonces().isEmpty()) {
			return false;
		}else {
			return true;
		}
	}
	
	
	public boolean modifierprofil(UserBean uBean) {
		
		
		UserDto uToCompare = null;
		uBean.setId_user(currentUser.getId_user());
		
		if(!uBean.getLogin().equals(currentUser.getLogin())) {
			UserConnBean uCheck = new UserConnBean();
			UserCriteria criteria = new UserCriteria();
			criteria.setLogin(uCheck.getLogin());
			uToCompare = uServ.getUserByCriteria(criteria);
		}
		
		if (uToCompare == null) {
			UserDao uDao = tDao.transformUserBeantoDao(uBean);
			UserDto uDto = uServ.update(uDao);
			currentUser = uDto;
			return true;
		}else {
			return false;
		}
	}
	
	public boolean updateUserActivation(UserDto user) {
		UserDao userDao = tDao.transformUsertoDao(user);
		
		uServ.update(userDao);
		if(getAllUsers()) {
			return true;
		}
		
		return false;

			
		
		
	}
	
	public boolean getAllUsers() {
		
		userList = uServ.getAllUsers();
		if(!userList.isEmpty()) {
			return true;
		}
		
		return false;
		
	}

	public boolean adminConnexion(UserConnBean uConn) {
		AdminServiceDao adminServ = new AdminServiceDao();
		
		UserCriteria criteria = new UserCriteria();
		criteria.setLogin(uConn.getLogin());
		
		
		AdminDto admin = adminServ.getAdmin(criteria);
		
		if(admin != null && admin.getPassword().equals(uConn.getPassword())) {
			return true;
		}
		
		return false;
	}
}
