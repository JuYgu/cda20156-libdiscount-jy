package fr.afpa.libDiscount.model.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AnnonceCriteria {

	private Long user_id;
	private String nom;
	private String niveau;
	private String ville;
	private String librairie;
}
