package fr.afpa.libDiscount.model.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserBean {
	private long id_user;
	private String nom;
	private String prenom;
	private String librairie;
	private String adresse;
	private String mail;
	private String login;
	private String password;
	private boolean activation;
}
