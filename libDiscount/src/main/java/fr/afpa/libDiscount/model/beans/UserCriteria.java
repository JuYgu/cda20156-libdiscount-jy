package fr.afpa.libDiscount.model.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor

public class UserCriteria {

	private String login;
	private long id;
	
	
	public UserCriteria() {
		this.id = 0;
	}
}
