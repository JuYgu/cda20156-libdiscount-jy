<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
        <%@ taglib prefix="c" 
          uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ include file="nav.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Mes annonces</title>
<link href="css/profil.css" rel="stylesheet">
<link href="css/mes-annonces.css" rel="stylesheet">
</head>
<body>
<main>
<h1>Mes annonces</h1>
	<div class="container">
	
	<c:forEach var="annonce" items="${sessionScope.annoncesUser}">
		<div class="annonce">
			<c:forEach var="photo" items="${annonce.photos}">
				<div class="img">
					<img class="image" src="images\<c:out value="${photo.nom }"/>"  alt="${photo.description }" style="display : inline-block; width : 200px; height : 200px">
				</div>
			</c:forEach>
			<div class="lab">
				<label><b>Titre</b></label>
				<p> ${annonce.nom}</p>
			</div>
			<div class="lab">
				<label><b>Niveau Scolaire</b></label>
				<p> ${annonce.niveauScolaire.niveau}</p> 
			</div>
			<div class="lab">
				<label><b>Maison Edition</b></label>
				<p> ${annonce.maisonEdition}</p>
			</div>
			<div class="lab">
				<label><b>Prix</b></label>
				<p> ${annonce.prixUnitaire}</p>
			</div>
			<div class="lab">
				<label><b>Remise</b></label>
				<p> ${annonce.remise}</p>
			</div>
			<div class="lab">
				<label><b>QUantité</b></label>
				<p> ${annonce.quantite}</p>
			</div>
			<div class="lab">
				<label><b>Prix Total</b></label>
				<p> ${annonce.prixTotal}</p>
			</div>
			<form action="mes-annonces" method="GET">
				<input type='hidden' name='id_annonce'  value="${annonce.id_annonce}" />
				<input class="button" type="submit" value="Modifier">
			</form>
			<form action="mes-annonces" method="GET">
				<input type='hidden' name='supprimer'  value="true" />
				<input class="button" type="submit" value="Supprimer">
			</form>

			</div>
	</c:forEach>
	</div>
	</main>
</body>
</html>