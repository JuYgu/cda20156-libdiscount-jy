<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false"%>

        <%@ taglib prefix="c" 
          uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/connexion.css" rel="stylesheet">
    </head>
    <body>
  <div id="container">

            <c:if test="${!empty sessionScope.sessionUtilisateur}">
            	<c:redirect url="profil"/> "/> 
               <p class="succes">Vous êtes connecté(e) avec : ${sessionScope.sessionUtilisateur.getLogin()}</p>
           </c:if>
            
            <form action="connexion" method="POST">
                <h1>Connexion</h1>
                
                <label><b>Nom d'utilisateur</b></label>
                <input type="text" placeholder="Entrer le nom d'utilisateur" name="login" value="${user.login }" >
                <span class="erreur">${connForm.erreurs['login']}</span>

                <label><b>Mot de passe</b></label>
                <input type="password" placeholder="Entrer le mot de passe" name="password" value="${user.password}" >
                <span class="erreur">${connForm.erreurs['password']}</span>

                <input  class="button" type="submit" id='submit' value='LOGIN' >
                 <p class="${empty form.erreurs ? 'succes' : 'erreur'}" style="color : red">${connForm.resultat}</p> 
                <a id='submit' value='INCRIPTION' href="inscription" > INSCRIPTION</a>
            </form>

        </div>
    </body>
</html>
