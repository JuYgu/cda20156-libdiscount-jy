<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
    <%@ taglib prefix="c" 
          uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ include file="nav.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>profil</title>
<link href="css/profil.css" rel="stylesheet">
</head>
<body>
<main>
<c:set var="m" value="true"/>


	<h1> Bonjour ${sessionScope.sessionUtilisateur.login} </h1>

	<div class= container>

    <c:choose> 
        <c:when test="${modification }"> 
            <form action="profil?modifier=Validation" method="post" id="modifprofil">
        <div>
            <label><b>Login</b></label>
            <input type="text" placeholder="Entrer le nom d'utilisateur" name="login" value=" ${sessionScope.sessionUtilisateur.login}" >
            <span class="erreur">${UserForm.erreurs['login']}</span>
        </div>

            <div>
            <label><b>Mot de passe</b></label>
            <input type="password" placeholder="Entrer le mot de passe" name="password" value=" ${sessionScope.sessionUtilisateur.password}" >
            <span class="erreur">${UserForm.erreurs['password']}</span>
        </div>

            <div>
            <label><b>Librairie</b></label>
            <input type="text" placeholder="Entrer le nom de votre Librairie" name="librairie" value=" ${sessionScope.sessionUtilisateur.librairie}" >
            <span class="erreur">${UserForm.erreurs['librairie']}</span>
        </div>

        <div>
            <label><b>Adresse</b></label>
            <input type="text" placeholder="Entrer votre adresse" name="adresse" value=" ${sessionScope.sessionUtilisateur.adresse}" >
            <span class="erreur">${UserForm.erreurs['adresse']}</span>
        </div>

        <div>
            <label><b>Mail</b></label>
            <input type="text" placeholder="Entrer votre mail" name="mail" value=" ${sessionScope.sessionUtilisateur.mail}" >
            <span class="erreur">${UserForm.erreurs['mail']}</span>
        </div> 

        <div>
            <label><b>Nom </b></label>
            <input type="text" placeholder="Entrer votre nom" name="nom" value=" ${sessionScope.sessionUtilisateur.nom}" >
            <span class="erreur">${UserForm.erreurs['nom']}</span>
        </div>

        <div>
            <label><b>Prenom</b></label>
            <input type="text" placeholder="Entrer votre prénom" name="prenom" value=" ${sessionScope.sessionUtilisateur.prenom}" >
            <span class="erreur">${UserForm.erreurs['prenom']}</span>
        </div>     

        <div>
              <p class="${empty form.erreurs ? 'succes' : 'erreur'}" style="color : red">${inscrForm.resultat}</p>       
            <input type="submit" name="modifier" value="Valider">
        </div>    
            </form> 
        </c:when>
        <c:otherwise> 
            <div>
                <label><b>Login</b></label>
            <p>  ${sessionScope.sessionUtilisateur.login} </p>
            </div>
            <div>
                <label><b>Librairie</b></label>
                <p> ${sessionScope.sessionUtilisateur.librairie} </p>
            </div>
            <div>
                <label><b>Nom</b></label>
                <p> ${sessionScope.sessionUtilisateur.nom} </p>
            </div>
            <div>
                <label><b>Prenom</b></label>
                <p>  ${sessionScope.sessionUtilisateur.prenom} </p>
            </div>
            <div>
                <label><b>Adresse</b></label>
                <p>  ${sessionScope.sessionUtilisateur.adresse} </p>
            </div>
            <div>
                <label><b>Mail</b></label>
                <p> ${sessionScope.sessionUtilisateur.mail} </p>
            </div>
            <form action="profil?modifier=ok" method="get">
                <input type="submit" name="modifier" value="Modifier">
            </form>
        </c:otherwise>
    </c:choose>  

    
</div>

</main>
</body>
</html>