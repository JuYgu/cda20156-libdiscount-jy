<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  isELIgnored="false"%>
        <%@ taglib prefix="c" 
          uri="http://java.sun.com/jsp/jstl/core" %>
              <%@ include file="nav.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>creation Annonce</title>
<link href="css/connexion.css" rel="stylesheet">
</head>
<body>
<main>
<h1>Modification de mon Annnonce</h1>
 <div id="container">

                
            
            <form action="ControlModificationAnnonce?id_annonce=${currentAnnonce.id_annonce}" method="POST" >

                
                <label><b>Nom</b></label>
                <input type="text" placeholder="Entrer le nom d'utilisateur" name="nom" value="${currentAnnonce.nom }" >
                <span class="erreur">${AnnForm.erreurs['nom']}</span>

                <label><b>ISBN</b></label>
                <input type="text" placeholder="Entrer le mot de passe" name="isnb" value="${currentAnnonce.isbn}" >
                <span class="erreur">${AnnForm.erreurs['password']}</span>
                
               <label><b>Niveau Scolaire</b></label>
                <select id="nvScolaire-select" name="niveau">
                	<c:set var="compteur" value="0" scope="request" /> 
                	<c:forEach var="nvScolaire" items="${sessionScope.nvScolaire}">
                		 <option value="${nvScolaire.niveau}" > <c:out value="${nvScolaire.niveau}"/></option>
                		 ${compteur +1}
                	</c:forEach>
				</select>
                <span class="erreur">${AnnForm.erreurs['librairie']}</span> 
                
                <input type="hidden" name="myhiddenvalue" value="" />
                
                <label><b>Maison Edition</b></label>
                <input type="text" placeholder="Maison d'Edition" name="medition" value="${currentAnnonce.maisonEdition}" >
                <span class="erreur">${AnnForm.erreurs['adresse']}</span>
                
                <label><b>Date d'Edition</b></label>
                <input type="text" placeholder="Date Edition (dd/mm/YYYY)" name="dedition" value="${currentAnnonce.dateEdition}" >
                <span class="erreur">${AnnForm.erreurs['mail']}</span>
                
                <label><b>Prix </b></label>
                <input type="text"  name="prixu"  value="${currentAnnonce.prixUnitaire}" >
                <span class="erreur">${AnnForm.erreurs['nom']}</span>
                
                <label><b>Quantite</b></label>
                <input type="text" name="quantite"  value="${currentAnnonce.quantite}" >
                <span class="erreur">${AnnForm.erreurs['quantite']}</span>
                
                <label><b>Remise</b></label>
                <input type="text" placeholder="Entrer votre prénom" name="remise" value="${currentAnnonce.remise}" >
                <span class="erreur">${AnnForm.erreurs['remise']}</span>
                

                <label><b>Photo</b></label>
                <input type="file"  name="multiPartServlet" >
                <input type="text" placeholder="Entrer la description de votre image" name="description" value="" >
                <span class="erreur">${AnnForm.erreurs['description']}</span>

                <input  class="button" type="submit" id='submit' value='VALIDER' >
                 <p class="${empty AnnForm.erreurs ? 'succes' : 'erreur'}" style="color : red">${AnnForm.resultat}</p> 
            </form>
        </div>
       	</main>
</body>
</html>