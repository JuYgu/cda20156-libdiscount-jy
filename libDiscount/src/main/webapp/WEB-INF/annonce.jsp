<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
                <%@ taglib prefix="c" 
          uri="http://java.sun.com/jsp/jstl/core" %>
              <%@ include file="nav.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="css/mes-annonces.css" rel="stylesheet">
</head>
<body>
<main>
JOJO le DODO
<div class="annonce">
			<div class="lab">
				<label><b>Titre</b></label>
				<p> ${currentAnnonce.nom}</p>
			</div>
			<div class="lab">
				<p> ${currentAnnonce.niveauScolaire.niveau}</p> 
			</div>
			<div class="lab">
				<label><b>Maison Edition</b></label>
				<p> ${currentAnnonce.maisonEdition}</p>
			</div>
			<div class="lab">
				<label><b>Prix</b></label>
				<p> ${currentAnnonce.prixUnitaire}</p>
			</div>
			<div class="lab">
				<label><b>Remise</b></label>
				<p> ${currentAnnonce.remise}</p>
			</div>
			</div>
</main>
</body>
</html>