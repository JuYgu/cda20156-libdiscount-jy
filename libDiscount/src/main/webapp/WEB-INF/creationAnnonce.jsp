<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  isELIgnored="false"%>
        <%@ taglib prefix="c" 
          uri="http://java.sun.com/jsp/jstl/core" %>
              <%@ include file="nav.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>creation Annonce</title>
<link href="css/connexion.css" rel="stylesheet">
        <script>
            /* Cette fonction permet d'afficher une vignette pour chaque image sélectionnée */
            function readFilesAndDisplayPreview(files) {
                let imageList = document.querySelector('#list'); 
                imageList.innerHTML = "";
                
                for ( let file of files ) {
                    let reader = new FileReader();
                    
                    reader.addEventListener( "load", function( event ) {
                        let span = document.createElement('span');
                        span.innerHTML = '<img height="60" src="' + event.target.result + '" />';
                        imageList.appendChild( span );
                    });

                    reader.readAsDataURL( file );
                }
            }
        </script>
</head>
<body>
 <main> 
 <div id="container">

            <!-- zone de connexion -->
            
            <form action="creation-annonce" method="POST" enctype="multipart/form-data">
                <h1>Creation d'une annonce</h1>
                
                <label><b>Nom</b></label>
                <input type="text" placeholder="Entrer le nom d'utilisateur" name="nom" value="${annonce.nom }" >
                <span class="erreur">${AnnForm.erreurs['nom']}</span>

                <label><b>ISBN</b></label>
                <input type="text" placeholder="Entrer le mot de passe" name="isnb" value="${annonce.isbn}" >
                <span class="erreur">${AnnForm.erreurs['password']}</span>
                
               <label><b>Niveau Scolaire</b></label>
                <select id="nvScolaire-select" name="niveau">
                	<c:set var="compteur" value="0" scope="request" /> 
                	<c:forEach var="nvScolaire" items="${sessionScope.nvScolaires}">
                		 <option value="${nvScolaire.niveau}" > <c:out value="${nvScolaire.niveau}"/></option>
                	</c:forEach>
				</select>
                <span class="erreur">${AnnForm.erreurs['librairie']}</span> 
                
                
                <label><b>Maison Edition</b></label>
                <input type="text" placeholder="Entrer votre adresse" name="medition" value="${annonce.adresse}" >
                <span class="erreur">${AnnForm.erreurs['adresse']}</span>
                
                <label><b>Date d'Edition</b></label>
                <input type="text" placeholder="Entrer votre mail" name="dedition" value="${annonce.dedition}" >
                <span class="erreur">${AnnForm.erreurs['mail']}</span>
                
                <label><b>Prix </b></label>
                <input type="text"  name="prixu"  value="${annonce.prixu}" >
                <span class="erreur">${AnnForm.erreurs['nom']}</span>
                
                <label><b>Quantite</b></label>
                <input type="text" name="quantite"  value="${annonce.quantite}" >
                <span class="erreur">${AnnForm.erreurs['quantite']}</span>
                
                <label><b>Remise</b></label>
                <input type="text" placeholder="Entrer votre prénom" name="remise" value="${annonce.remise}" >
                <span class="erreur">${AnnForm.erreurs['remise']}</span>
                

                <label><b>Photo</b></label>
                <input type="file"  name="multiPartServlet" accept="image/*" multiple onchange="readFilesAndDisplayPreview(this.files);" >
                <input type="text" placeholder="Entrer la description de votre image" name="description" value="${annonce.description}" >
                <span class="erreur">${AnnForm.erreurs['description']}</span>

                <input  class="button" type="submit" id='submit' value='VALIDER' >
                 <p class="${empty AnnForm.erreurs ? 'succes' : 'erreur'}" style="color : red">${AnnForm.resultat}</p> 
            </form>
        </div>
    </main>
</body>
</html>