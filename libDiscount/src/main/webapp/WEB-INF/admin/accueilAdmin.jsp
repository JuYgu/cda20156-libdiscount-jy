<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  isELIgnored="false"%>
        <%@ taglib prefix="c" 
          uri="http://java.sun.com/jsp/jstl/core" %>
              <%@ include file="../navAdmin.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Bonjour Monsieur l'admin</title>
<link href="css/admin.css" rel="stylesheet">
</head>
<body>
<main>
	<div class="container">
	<table>
		<thead> 
			<tr> 
				
				<th> Login </th>
				<th> Description  </th>
				<th> Active  </th>
				<th> Actions </th>
			</tr>
			
		</thead>
		<tbody>
			<c:forEach var="user" items="${userList}">
				<tr> 
					<td> <a href="admin?id_user=${user.id_user }"> ${ user.login }</a></td>
					<td>  ${ user.nom  }  ${user.prenom } , ${user.librairie }</td>
					<td>  ${ user.activation }</td>
					<td> 
					<c:set var="activation" value="${user.activation}"></c:set>
						<c:choose>
							<c:when test= "${activation eq true}">
								<a class="btn-desactiver"  href="admin?id_user=${user.id_user }&activation=false"> Desactiver </a>
							</c:when>
							<c:otherwise >
								<a class="btn-activer" href="admin?id_user=${user.id_user }&activation=true"> Activer </a>
							</c:otherwise>
						 </c:choose>
					</td>
				</tr>
			</c:forEach>	
		</tbody>
	</table>
	</div>
</main>

</body>
</html>