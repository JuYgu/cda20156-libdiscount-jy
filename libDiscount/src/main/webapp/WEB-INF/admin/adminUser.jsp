<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  isELIgnored="false"%>
        <%@ taglib prefix="c" 
          uri="http://java.sun.com/jsp/jstl/core" %>
              <%@ include file="../navAdmin.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Bonjour Monsieur l'admin</title>
<link href="css/admin.css" rel="stylesheet">
</head>
<body>
<main>
	<div class="container">

	<table>
		<thead> 
			<tr> 
				
				<th> Titre </th>
				<th> Description  </th>
				<th> Active  </th>
				<th> Prix </th>
				<th> Actions </th>
			</tr>
			
		</thead>
		<tbody>
			<c:forEach var="annonce" items="${currentUser.annonces}">
				<tr> 
					<td> <a href="admin?id_user=${annonce.id_annonce }"> ${ annonce.nom }</a></td>
					<td>  ${ annonce.maisonEdition  }  ${annonce.dateEdition } , ${annonce.prixUnitaire }</td>
					<td>  ${ annonce.activation }</td>
					<td>  ${ annonce.prixTotal }</td>
					<td> 
						<c:choose>
							<c:when test="${annonce.activation}">
								<a class="btn-desactiver"  href="admin?id_user=${currentUser.id_user}&id_annonce=${annonce.id_annonce }&activation=false"> Desactiver </a>
							</c:when>
							<c:otherwise>
								<a class="btn-activer" href="admin?id_user=${currentUser.id_user}&id_annonce=${annonce.id_annonce }&activation=true"> Activer </a>
							</c:otherwise>
						 </c:choose>
					</td>
				</tr>
			</c:forEach>	
		</tbody>
	</table>
	</div>
</main>

</body>
</html>