<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  isELIgnored="false"%>
        <%@ taglib prefix="c" 
          uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

		<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet"> 
		      <!--   <link href="css/connexion.css" rel="stylesheet"> -->
        <link href="css/nav.css" rel="stylesheet">

</head>
<body>
	<header>
	<nav>
		<ul id="nav">
			<li><a href="<c:url value="accueil"/>">Accueil</a></li>
			<li><a href="<c:url value ="profil"/>">Mon Profil</a></li>
			<li><a href="<c:url value="mes-annonces"/>">Mes Annonces</a></li>
			<li><a href="<c:url value="creation-annonce"/>">Créer une annononce</a></li>
			<li><a href="<c:url value="deconnexion"/>">Déconnexion</a></li>
		 </ul>
	</nav>
	</header>

</body>
</html>