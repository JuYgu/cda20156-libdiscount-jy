package hibernate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.afpa.libDiscount.dao.beans.AnnonceDao;
import fr.afpa.libDiscount.dao.beans.NiveauScolaireDao;
import fr.afpa.libDiscount.dao.beans.PhotoDao;
import fr.afpa.libDiscount.dao.beans.UserDao;
import fr.afpa.libDiscount.dao.session.HibernateUtils;

public class HibernateTest {

	private Session s;
	public static UserDao u;
	public static AnnonceDao a;
	
	@Before
	public void Before() {
		 s = HibernateUtils.getSession();
		 
	}
	
	@Test
	public void TestAddUser() {
		UserDao user = new UserDao();
		user.setPrenom("prenom");
		user.setNom("nom");
		user.setAdresse("adresse");
		user.setActivation(true);
		user.setLibrairie("librairie");
		user.setLogin("login");
		user.setPassword("password");
		user.setMail("mail");
		
		Transaction tx = s.beginTransaction();
	  	s.save(user);

	  	
	  	List<UserDao> users = s.createQuery("SELECT a FROM UserDao a", UserDao.class).getResultList();
	  	tx.commit();
	  	
	  	u = users.get(0);
	  	System.out.println(u.getId_user());
	  	assertNotNull(users);
	}
	
	@Test
	public void TestAddAnnonce() {
		AnnonceDao annonce = new AnnonceDao();
		annonce.setActivation(true);
		annonce.setDateEdition(LocalDate.now());
		annonce.setIsbn("1111111111111");
		annonce.setMaisonEdition("MaisonEdition");
		annonce.setNom("livre");
		annonce.setPrixUnitaire(10);
		annonce.setRemise(0);
		annonce.setQuantite(1);
		annonce.setPrixTotal(annonce.getPrixTotal());
		annonce.setUser(u);
//		
		Transaction tx = s.beginTransaction();
	  	s.save(annonce);

	  	
	  	List<AnnonceDao> annonces = s.createQuery("SELECT a FROM AnnonceDao a", AnnonceDao.class).getResultList();
	  	tx.commit();
	  	
	  	a = annonces.get(0);
	  	assertNotNull(annonces);
	  
	}
	
	
	@Test
	public void TestAddPhoto() {
		PhotoDao photo = new PhotoDao();
		photo.setDescription("une photo");
		photo.setPath("C:/ENV/workspace/libDiscount/src/test/resources/images/pamp.jpg");
		photo.setAnnonce(a);
		photo.setNom("jojo.jpg");
//		
		 Transaction tx = s.beginTransaction();
		 s.save(photo);
		
		 
		 List<PhotoDao> photos = s.createQuery("SELECT a FROM PhotoDao a", PhotoDao.class).getResultList();
		 tx.commit();
		 
		 assertNotNull(photos);
	}
	
	@Test
	public void TestAddNiveauScolaire() {
		NiveauScolaireDao nv = new NiveauScolaireDao();
		nv.setNiveau("6eme");
		NiveauScolaireDao nv2 = new NiveauScolaireDao();
		nv2.setNiveau("5eme");
		NiveauScolaireDao nv3 = new NiveauScolaireDao();
		nv3.setNiveau("Supérieur");
		
		List<NiveauScolaireDao> niveaux = new ArrayList<>();
		niveaux.add(nv);
		niveaux.add(nv2);
		niveaux.add(nv3);
//		
		Transaction tx = s.beginTransaction();
//		
		for ( NiveauScolaireDao niv : niveaux) {
			s.save(niv);
		}
//		
		
		List<NiveauScolaireDao> nvx = s.createQuery("SELECT a FROM NiveauScolaireDao a", NiveauScolaireDao.class).getResultList();
		tx.commit();
	
		assertNotNull(nvx);
	}
	
	 @After
	 public void after() {
		 s.close();
	 }
	 
	
}
