package dto;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.modelmapper.ModelMapper;

import fr.afpa.libDiscount.dao.beans.AnnonceDao;
import fr.afpa.libDiscount.dao.beans.NiveauScolaireDao;
import fr.afpa.libDiscount.dao.beans.PhotoDao;
import fr.afpa.libDiscount.dao.beans.UserDao;
import fr.afpa.libDiscount.dao.session.HibernateUtils;
import fr.afpa.libDiscount.dto.TransformToDto;
import fr.afpa.libDiscount.dto.beans.UserDto;

public class MappingTransferTest {

	UserDao user;
	AnnonceDao annonce;
	PhotoDao photo;
	NiveauScolaireDao niveau;
	Session s = null;
	TransformToDto tutd;
	
	@Before
	public void before() {
		
		user = new UserDao();
		user.setPrenom("prenom");
		user.setNom("nom");
		user.setAdresse("adresse");
		user.setActivation(true);
		user.setLibrairie("librairie");
		user.setLogin("login");
		user.setPassword("password");
		user.setMail("mail");
		
		NiveauScolaireDao nv = new NiveauScolaireDao();
		nv.setNiveau("6eme");
		
		annonce = new AnnonceDao();
		annonce.setActivation(true);
		annonce.setDateEdition(LocalDate.now());
		annonce.setIsbn("1111111111111");
		annonce.setMaisonEdition("MaisonEdition");
		annonce.setNom("livre");
		annonce.setPrixUnitaire(10);
		annonce.setRemise(0);
		annonce.setQuantite(1);
		annonce.setPrixTotal(annonce.getPrixTotal());
		annonce.setUser(user);
		annonce.setNiveauScolaire(nv);
		
		photo = new PhotoDao();
		photo.setDescription("une photo");
		photo.setPath("C:/ENV/workspace/libDiscount/src/test/resources/images/pamp.jpg");
		photo.setNom("pamp.jpg");
		photo.setAnnonce(annonce);
		
		
		 List<PhotoDao> p = new ArrayList<>();
		 p.add(photo);
		 annonce.setPhotos(p);
		 
		 List<AnnonceDao> la = new ArrayList<>();
		 la.add(annonce);
		 user.setAnnonces(la);
		 
		 s = HibernateUtils.getSession();
		 Transaction tx = s.beginTransaction();
		 s.save(user);
		 s.save(annonce);
		 s.save(photo);
		 s.save(nv);
		 tx.commit();
	}
	
	@Test
	public void testTransformUsertoDto() {
		Transaction tx = s.beginTransaction();
	  	List<UserDao> users = s.createQuery("SELECT a FROM UserDao a", UserDao.class).getResultList();
	  	tx.commit();
	  	
	  	//tutd = new TransformUserToDto();
	  	
	  //	List<UserDto> usersDto = tutd.transformUsertoDto(users);
		ModelMapper mp = new ModelMapper();
		
		List<UserDto> dtoUsers = new ArrayList<>();
		
		for( UserDao daoUser : users) {
			UserDto user = mp.map(daoUser, UserDto.class);
			dtoUsers.add(user);
		}
		
		System.err.println(dtoUsers.get(0));
	  	
	  	assertEquals(users.size(), dtoUsers.size());
	  	assertEquals(users.get(0).getLibrairie(), dtoUsers.get(0).getLibrairie());
	}
	

}
