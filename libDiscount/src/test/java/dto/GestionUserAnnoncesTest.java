package dto;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;

import fr.afpa.libDiscount.dao.AnnonceServiceDao;
import fr.afpa.libDiscount.dao.UserServiceDao;
import fr.afpa.libDiscount.dao.beans.AnnonceDao;
import fr.afpa.libDiscount.dao.beans.NiveauScolaireDao;
import fr.afpa.libDiscount.dao.beans.PhotoDao;
import fr.afpa.libDiscount.dao.beans.UserDao;
import fr.afpa.libDiscount.dao.session.HibernateUtils;
import fr.afpa.libDiscount.dto.beans.AnnonceDto;
import fr.afpa.libDiscount.dto.beans.UserDto;
import fr.afpa.libDiscount.model.GestionUser;
import fr.afpa.libDiscount.model.beans.AnnonceCriteria;
import fr.afpa.libDiscount.model.beans.UserBean;
import fr.afpa.libDiscount.model.beans.UserCriteria;

public class GestionUserAnnoncesTest {

	private UserBean uBean;
	private UserCriteria criteria;
	private UserDao user;
	private UserDto uDto;
	private GestionUser gUser;
	private UserServiceDao servDao;
	private AnnonceDao annonce;
	private Session s;
	
	@Before
	public void before() {
		
		s = HibernateUtils.getSession();
		Transaction tx = s.beginTransaction();
		
		
		
		user = new UserDao();
		user.setPrenom("prenom");
		user.setNom("nom");
		user.setAdresse("adresse");
		user.setActivation(true);
		user.setLibrairie("librairie");
		user.setLogin("lo");
		user.setPassword("password");
		user.setMail("mail");
		
		NiveauScolaireDao nv = new NiveauScolaireDao();
		nv.setNiveau("6eme");
		
		annonce = new AnnonceDao();
		annonce.setActivation(true);
		annonce.setDateEdition(LocalDate.now());
		annonce.setIsbn("1111111111111");
		annonce.setMaisonEdition("MaisonEdition");
		annonce.setNom("livre");
		annonce.setPrixUnitaire(10);
		annonce.setRemise(0);
		annonce.setQuantite(1);
		annonce.setPrixTotal(annonce.getPrixTotal());
		annonce.setUser(user);
		annonce.setNiveauScolaire(nv);
		
	
		PhotoDao photo = new PhotoDao();
		photo.setDescription("une photo");
		photo.setPath("C:/ENV/workspace/libDiscount/src/test/resources/images/pamp.jpg");
		photo.setAnnonce(annonce);
		photo.setActivation(true);
		photo.setNom("pamp.jpg");
		 List<PhotoDao> p = new ArrayList<>();
		 p.add(photo);
		 annonce.setPhotos(p);
		 
		 List<AnnonceDao> la = new ArrayList<>();
		 la.add(annonce);
		 user.setAnnonces(la);
		 s.save(nv);
		 s.save(photo);
		//s.save(user);

		tx.commit();
		s.close();
	}
	

	public void testGetUserByLogin() {
		uBean = new UserBean();
		uBean.setLogin("logo");
		criteria = new UserCriteria();
		criteria.setLogin(uBean.getLogin());
		servDao = new UserServiceDao();
		uDto = servDao.getUserByCriteria(criteria);
		
		assertNotNull(uDto);
		assertEquals(user.getLogin(), uDto.getLogin());
		
	}
	
	
	public void testGetUserById() {
		uBean = new UserBean();
		uBean.setId_user(2);
		criteria = new UserCriteria();
		criteria.setId(uBean.getId_user());
		servDao = new UserServiceDao();
		uDto = servDao.getUserByCriteria(criteria);
		
		assertNotNull(uDto);
		assertNotEquals(user.getId_user(), uDto.getId_user());
		
	}
	
	@Test
	public void testCreateUser() {
		servDao = new UserServiceDao();
		uDto = servDao.create(user);
		assertNotNull(uDto);
		assertEquals(user.getId_user(), uDto.getId_user());
	}
	
	@Test
	public void TestGetAnnonceUser() {
		servDao = new UserServiceDao();
		UserCriteria criteria = new UserCriteria();
		criteria.setId(28);
		uDto = servDao.getUserByCriteria(criteria); 
		System.err.println("Udto : "+uDto);
		
		
		assertEquals(1, uDto.getAnnonces().size());
		assertEquals(uDto.getAnnonces().get(0).getMaisonEdition(), annonce.getMaisonEdition());
	}
	
@Test
	public void testGetAnnonceByVille() {
		AnnonceServiceDao aServDao = new AnnonceServiceDao();
		AnnonceCriteria annCriteria = new AnnonceCriteria();
		annCriteria.setVille("ad");
		List<AnnonceDto> lDto = aServDao.getAnnoncesByCriteria(annCriteria);
		System.err.println(lDto);
		assertEquals(annonce.getMaisonEdition(), lDto.get(0).getMaisonEdition());
		
	}
	
	
}
