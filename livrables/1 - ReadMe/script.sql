insert into niveauscolairedao values (  nextval('nscolaire_seq'), 'primaire'), (  nextval('nscolaire_seq'), 'secondaire'), (  nextval('nscolaire_seq'), 'superieur') ;

insert into admindao values (nextval('user_seq'), true, 'test adresse admin', 'test librairie admin', 'admin', 'admin@gmail.com', 'admin', 'cda', 'admin');

CREATE TABLE public.archivage (
	id_archivage int8 not null,
	id_annonce int8 NOT NULL,
	activation bool NOT NULL,
	dateedition date NOT NULL,
	isbn varchar(13) NOT NULL,
	maisonedition varchar(255) NOT NULL,
	nom varchar(255) NOT NULL,
	prixtotal float4 NOT NULL,
	prixunitaire float4 NOT NULL,
	quantite int4 NOT NULL,
	remise float4 NOT NULL,
	fk_id_niveauscolaire int8 NULL,
	fk_id_user int8 NULL
);

create SEQUENCE public.achivage_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;

create function BACKUP_ANNONCE() returns trigger as $$
begin
	insert into public.archivage VALUES( nextval('achivage_seq'), old.id_annonce, old.activation, old.dateedition, old.isbn, old.maisonedition, old.nom, old.prixtotal, old.prixunitaire, old.quantite, old.remise, old.fk_id_niveauscolaire, old.fk_id_user);
	return old;
end;
$$
language plpgsql;

create trigger "moveToArchive"
before delete 
on public.annoncedao 
for each row 
execute procedure BACKUP_ANNONCE();
